package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"flag"
	"io/ioutil"
	"log"
	"os"

	"github.com/google/subcommands"
)

type genKeyCmd struct {
	keyPath string
}

func (c *genKeyCmd) Name() string     { return "gen-key" }
func (c *genKeyCmd) Synopsis() string { return "Generate an EC key" }
func (c *genKeyCmd) Usage() string {
	return `gen-key [<options>]

Generate a new EC key.

`
}

func (c *genKeyCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.keyPath, "key", "", "Private key path")
}

func (c *genKeyCmd) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	pkey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Printf("failed to generate ECDSA key: %v", err)
		return subcommands.ExitFailure
	}

	data, err := MarshalPrivateKey(pkey)
	if err != nil {
		log.Printf("ERROR: could not marshal private key: %v", err)
		return subcommands.ExitFailure
	}

	if c.keyPath != "" {
		if err := ioutil.WriteFile(c.keyPath, data, 0600); err != nil {
			log.Printf("ERROR: could not save private key: %v", err)
			return subcommands.ExitFailure
		}
	} else {
		os.Stdout.Write(data)
	}
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&genKeyCmd{}, "")
}
