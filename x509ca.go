package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"

	"github.com/google/subcommands"
)

const filesModifiedExitStatus = 10

var (
	useCustomExitStatus = flag.Bool("use-exit-status", false, fmt.Sprintf("Use a custom exit status (%d) to indicate that certificates have been modified", filesModifiedExitStatus))
)

const (
	pemCertificateType        = "CERTIFICATE"
	pemECPrivateKeyType       = "EC PRIVATE KEY"
	pemCertificateRequestType = "CERTIFICATE REQUEST"
)

// MarshalCertificate encodes a certificate in PEM format.
func MarshalCertificate(cert *x509.Certificate) []byte {
	return pem.EncodeToMemory(&pem.Block{Type: pemCertificateType, Bytes: cert.Raw})
}

// MarshalCertificateRequest encodes a csr in PEM format.
func MarshalCertificateRequest(csr *x509.CertificateRequest) []byte {
	return pem.EncodeToMemory(&pem.Block{Type: pemCertificateRequestType, Bytes: csr.Raw})
}

// MarshalPrivateKey encodes a private key in PEM format.
func MarshalPrivateKey(priv *ecdsa.PrivateKey) ([]byte, error) {
	der, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		return nil, err
	}
	data := pem.EncodeToMemory(&pem.Block{Type: pemECPrivateKeyType, Bytes: der})
	return data, nil
}

// UnmarshalCertificate reads a single X509 certificate encoded in PEM
// format from the given data.
func UnmarshalCertificate(data []byte) (*x509.Certificate, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("no PEM block found in input")
	}
	if block.Type != pemCertificateType {
		return nil, errors.New("encoded object is not a certificate")
	}
	certs, err := x509.ParseCertificates(block.Bytes)
	if err != nil {
		return nil, err
	}
	return certs[0], nil
}

// UnmarshalCertificateRequest reads a PEM-encoded CSR.
func UnmarshalCertificateRequest(data []byte) (*x509.CertificateRequest, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("no PEM block found in input")
	}
	if block.Type != pemCertificateRequestType {
		return nil, errors.New("encoded object is not a certificate request")
	}
	return x509.ParseCertificateRequest(block.Bytes)
}

// UnmarshalPrivateKey reads a PEM-encoded private key.
func UnmarshalPrivateKey(data []byte) (*ecdsa.PrivateKey, error) {
	block, _ := pem.Decode(data)
	if block == nil {
		return nil, errors.New("no PEM block found in input")
	}
	if block.Type != pemECPrivateKeyType {
		return nil, fmt.Errorf("encoded object is not an EC private key (%s)", block.Type)
	}
	return x509.ParseECPrivateKey(block.Bytes)
}

func loadCertificate(path string) (*x509.Certificate, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return UnmarshalCertificate(data)
}

func loadPrivateKey(path string) (*ecdsa.PrivateKey, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	return UnmarshalPrivateKey(data)
}

func saveCertificate(cert *x509.Certificate, path string) error {
	return ioutil.WriteFile(path, MarshalCertificate(cert), 0644)
}

func savePrivateKey(pkey *ecdsa.PrivateKey, path string) error {
	data, err := MarshalPrivateKey(pkey)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path, data, 0600)
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "")
	subcommands.Register(subcommands.FlagsCommand(), "")
	subcommands.Register(subcommands.CommandsCommand(), "")
}

func exitStatus(changed, checkOnly bool) subcommands.ExitStatus {
	if !checkOnly && *useCustomExitStatus && changed {
		return subcommands.ExitStatus(filesModifiedExitStatus)
	}
	return subcommands.ExitSuccess
}

func main() {
	log.SetFlags(0)
	flag.Parse()
	syscall.Umask(077)
	os.Exit(int(subcommands.Execute(context.Background())))
}
