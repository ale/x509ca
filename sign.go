package main

import (
	"context"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/google/subcommands"
)

type signCmd struct {
	csrPath      string
	caCertPath   string
	caKeyPath    string
	isClient     bool
	isServer     bool
	validityDays int
}

func (c *signCmd) Name() string     { return "sign" }
func (c *signCmd) Synopsis() string { return "Sign a CSR" }
func (c *signCmd) Usage() string {
	return `sign [<options>]

Sign a new certificate.

`
}

func (c *signCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.csrPath, "csr", "", "CSR path")
	f.StringVar(&c.caCertPath, "ca-cert", "", "CA certificate path")
	f.StringVar(&c.caKeyPath, "ca-key", "", "CA private key path")
	f.BoolVar(&c.isClient, "client", false, "Enable client mode for the certificate")
	f.BoolVar(&c.isServer, "server", false, "Enable server mode for the certificate")
	f.IntVar(&c.validityDays, "validity", 365, "Validity of the new certificate (days)")
}

func (c *signCmd) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if c.caCertPath == "" || c.caKeyPath == "" {
		log.Printf("ERROR: --ca-cert and --ca-key must be specified")
		return subcommands.ExitFailure
	}
	if !c.isClient && !c.isServer {
		log.Printf("ERROR: --client and/or --server must be specified")
		return subcommands.ExitFailure
	}

	// Load from standard input if no --csr argument given.
	var csrData []byte
	var err error
	if c.csrPath != "" {
		csrData, err = ioutil.ReadFile(c.csrPath)
	} else {
		csrData, err = ioutil.ReadAll(os.Stdin)
	}
	if err != nil {
		log.Printf("ERROR: %v", err)
		return subcommands.ExitFailure
	}

	csr, err := UnmarshalCertificateRequest(csrData)
	if err != nil {
		log.Printf("ERROR: parsing CSR: %v", err)
		return subcommands.ExitFailure
	}

	// Load CA and sign certificate.
	ca, err := loadCA(c.caCertPath, c.caKeyPath)
	if err != nil {
		log.Printf("ERROR: could not load CA: %v", err)
		return subcommands.ExitFailure
	}

	cert, err := ca.signCSR(csr, c.isClient, c.isServer, time.Duration(c.validityDays)*oneDay)
	if err != nil {
		log.Printf("ERROR: could not sign certificate: %v", err)
		return subcommands.ExitFailure
	}

	os.Stdout.Write(MarshalCertificate(cert))
	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&signCmd{}, "")
}
