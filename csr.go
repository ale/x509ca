package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/x509"
	"flag"
	"fmt"
	"log"

	"github.com/google/subcommands"
)

type csrCmd struct {
	subject pkixNameFlag
	csrPath string
	keyPath string
	sanList stringListFlag
	ipList  ipListFlag
}

func (c *csrCmd) Name() string     { return "csr" }
func (c *csrCmd) Synopsis() string { return "Generate a CSR" }
func (c *csrCmd) Usage() string {
	return `csr [<options>]

Generate a certificate signing request.

`
}

func (c *csrCmd) SetFlags(f *flag.FlagSet) {
	f.Var(&c.subject, "subject", "Certificate subject (in CN=.../OU=.../etc format)")
	f.StringVar(&c.csrPath, "csr", "", "Output CSR `path`")
	f.StringVar(&c.keyPath, "key", "private_key.pem", "Private key `path`")
	f.Var(&c.sanList, "alt", "Add `subjectAltName` to the certificate")
	f.Var(&c.ipList, "ip", "Add an `IP` to the certificate")
}

func (c *csrCmd) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if c.subject.Name == nil {
		log.Printf("ERROR: --subject must be specified")
		return subcommands.ExitUsageError
	}
	if c.subject.Name.CommonName == "" {
		log.Printf("ERROR: --subject must include a CN")
		return subcommands.ExitUsageError
	}

	priv, err := loadPrivateKey(c.keyPath)
	if err != nil {
		log.Printf("ERROR: could not load private key: %v", err)
		return subcommands.ExitFailure
	}

	// Ensure that the CN is part of the subjectAltNames.
	c.sanList.Set(c.subject.Name.CommonName)

	csr, err := c.makeCertificateRequest(priv)
	if err != nil {
		log.Printf("ERROR: could not create certificate signing request: %v", err)
		return subcommands.ExitFailure
	}

	data := MarshalCertificateRequest(csr)
	fmt.Printf("%s\n", data)

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&csrCmd{}, "")
}

func (c *csrCmd) makeCertificateRequest(priv *ecdsa.PrivateKey) (*x509.CertificateRequest, error) {
	b, err := x509.CreateCertificateRequest(
		rand.Reader,
		&x509.CertificateRequest{
			Subject:            c.subject.PKIXName(),
			DNSNames:           c.sanList,
			IPAddresses:        c.ipList,
			SignatureAlgorithm: x509.ECDSAWithSHA256,
		},
		priv,
	)
	if err != nil {
		return nil, err
	}
	return x509.ParseCertificateRequest(b)
}
