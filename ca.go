package main

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"fmt"
	"log"
	"math/big"
	"time"

	"github.com/google/subcommands"
)

// CA holds the private key for our certification authority.
type CA struct {
	pkey *ecdsa.PrivateKey
	cert *x509.Certificate
}

func loadCA(certPath, keyPath string) (*CA, error) {
	pkey, err := loadPrivateKey(keyPath)
	if err != nil {
		return nil, err
	}
	cert, err := loadCertificate(certPath)
	if err != nil {
		return nil, err
	}
	return &CA{pkey: pkey, cert: cert}, nil
}

func renewCA(ca *CA, certPath string, subj *pkix.Name) (*CA, error) {
	return signCA(ca.pkey, certPath, subj)
}

func newCA(certPath, keyPath string, subj *pkix.Name) (*CA, error) {
	pkey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, fmt.Errorf("failed to generate ECDSA private key: %v", err)
	}
	if err := savePrivateKey(pkey, keyPath); err != nil {
		return nil, err
	}

	return signCA(pkey, certPath, subj)
}

func signCA(pkey *ecdsa.PrivateKey, certPath string, subj *pkix.Name) (*CA, error) {
	now := time.Now().UTC()
	template := x509.Certificate{
		SerialNumber:          big.NewInt(1),
		Subject:               *subj,
		NotBefore:             now.Add(-5 * time.Minute),
		NotAfter:              now.AddDate(10, 0, 0), // 10 years.
		SignatureAlgorithm:    x509.ECDSAWithSHA256,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
		IsCA:                  true,
		MaxPathLen:            1,
	}
	der, err := x509.CreateCertificate(rand.Reader, &template, &template, pkey.Public(), pkey)
	if err != nil {
		return nil, fmt.Errorf("could not self-sign CA certificate: %v", err)
	}
	cert, err := x509.ParseCertificate(der)
	if err != nil {
		// This would be weird.
		return nil, err
	}

	// Save the new files.
	if err := saveCertificate(cert, certPath); err != nil {
		return nil, err
	}

	return &CA{pkey: pkey, cert: cert}, nil
}

func (ca *CA) signCSR(csr *x509.CertificateRequest, isClient, isServer bool, validity time.Duration) (*x509.Certificate, error) {
	if err := csr.CheckSignature(); err != nil {
		return nil, err
	}

	tpl, err := templateFromCSR(csr, isClient, isServer, validity)
	if err != nil {
		return nil, err
	}

	der, err := x509.CreateCertificate(rand.Reader, tpl, ca.cert, csr.PublicKey, ca.pkey)
	if err != nil {
		return nil, err
	}
	return x509.ParseCertificate(der)
}

func templateFromCSR(csr *x509.CertificateRequest, isClient, isServer bool, validity time.Duration) (*x509.Certificate, error) {
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, fmt.Errorf("failed to generate serial number: %v", err)
	}

	var extUsage []x509.ExtKeyUsage
	if isServer {
		extUsage = append(extUsage, x509.ExtKeyUsageServerAuth)
	}
	if isClient {
		extUsage = append(extUsage, x509.ExtKeyUsageClientAuth)
	}
	now := time.Now().UTC()

	return &x509.Certificate{
		SerialNumber:          serialNumber,
		Subject:               csr.Subject,
		IPAddresses:           csr.IPAddresses,
		DNSNames:              csr.DNSNames,
		EmailAddresses:        csr.EmailAddresses,
		NotBefore:             now.Add(-5 * time.Minute),
		NotAfter:              now.Add(validity),
		SignatureAlgorithm:    csr.SignatureAlgorithm,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           extUsage,
		PublicKey:             csr.PublicKey,
		BasicConstraintsValid: true,
	}, nil
}

type initCmd struct {
	subject    pkixNameFlag
	caCertPath string
	caKeyPath  string
	checkOnly  bool
	renewDays  int
}

func (c *initCmd) Name() string     { return "init" }
func (c *initCmd) Synopsis() string { return "Initialize the CA" }
func (c *initCmd) Usage() string {
	return `init [<options>]

Initialize the CA. If called more than once, it will check
for CA validity and eventually renew its public certificate
before it expires.

`
}

func (c *initCmd) SetFlags(f *flag.FlagSet) {
	f.Var(&c.subject, "subject", "CA subject (in CN=.../OU=.../etc format)")
	f.StringVar(&c.caCertPath, "ca-cert", "", "CA certificate `path`")
	f.StringVar(&c.caKeyPath, "ca-key", "", "CA private key `path`")
	f.BoolVar(&c.checkOnly, "check", false, "Only check if initialization or renewal should be performed")
	f.IntVar(&c.renewDays, "renew-days", 60, "How many `days` to warn in advance that a certificate is about to expire")
}

func (c *initCmd) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if c.caCertPath == "" || c.caKeyPath == "" {
		log.Printf("ERROR: --ca-cert and --ca-key must be specified")
		return subcommands.ExitFailure
	}
	if c.subject.Name == nil {
		log.Printf("ERROR: --subject must be specified")
		return subcommands.ExitFailure
	}

	changed := false
	ca, err := loadCA(c.caCertPath, c.caKeyPath)
	if err != nil {
		if c.checkOnly {
			return subcommands.ExitFailure
		}
		log.Printf("generating new CA certificate for %s", pkixNameToString(*c.subject.Name))
		ca, err = newCA(c.caCertPath, c.caKeyPath, c.subject.Name)
		if err != nil {
			log.Printf("ERROR: could not create new CA: %v", err)
			return subcommands.ExitFailure
		}
		changed = true
	}
	if aboutToExpire(ca.cert, c.renewDays) {
		if c.checkOnly {
			return subcommands.ExitFailure
		}
		log.Printf("renewing CA certificate for %s", pkixNameToString(*c.subject.Name))
		_, err = renewCA(ca, c.caCertPath, c.subject.Name)
		if err != nil {
			log.Printf("ERROR: could not renew CA certificate: %v", err)
			return subcommands.ExitFailure
		}
		changed = true
	}
	return exitStatus(changed, c.checkOnly)
}

func init() {
	subcommands.Register(&initCmd{}, "")
}
