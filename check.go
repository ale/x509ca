package main

import (
	"context"
	"crypto/x509"
	"crypto/x509/pkix"
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/google/subcommands"
)

type checkCmd struct {
	subject    pkixNameFlag
	certPath   string
	caCertPath string
	sanList    stringListFlag
	ipList     ipListFlag
	isClient   bool
	isServer   bool
	renewDays  int
}

func (c *checkCmd) Name() string     { return "check" }
func (c *checkCmd) Synopsis() string { return "Check if a certificate needs to be updated" }
func (c *checkCmd) Usage() string {
	return `check [<options>]

Check if a certificate needs to be updated. It will check for
certificate metadata correctness (including changes in subjectAltName
or IP lists) and expiration time.

`
}

func (c *checkCmd) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.certPath, "cert", "", "Certificate `path`")
	f.StringVar(&c.caCertPath, "ca-cert", "", "CA certificate path")
	f.Var(&c.subject, "subject", "Certificate subject (in CN=.../OU=.../etc format)")
	f.Var(&c.sanList, "alt", "Add `subjectAltName` to the certificate")
	f.Var(&c.ipList, "ip", "Add an `IP` to the certificate")
	f.BoolVar(&c.isClient, "client", false, "Enable client mode for the certificate")
	f.BoolVar(&c.isServer, "server", false, "Enable server mode for the certificate")
	f.IntVar(&c.renewDays, "renew-days", 60, "How many `days` to warn in advance that a certificate is about to expire")
}

func (c *checkCmd) Execute(ctx context.Context, _ *flag.FlagSet, _ ...interface{}) subcommands.ExitStatus {
	if c.certPath == "" {
		log.Printf("ERROR: --cert must be specified")
		return subcommands.ExitUsageError
	}

	cert, err := loadCertificate(c.certPath)
	if err != nil {
		log.Printf("ERROR: could not load certificate: %v", err)
		return subcommands.ExitFailure
	}

	var caCert *x509.Certificate
	if c.caCertPath != "" {
		caCert, err = loadCertificate(c.caCertPath)
		if err != nil {
			log.Printf("ERROR: could not load CA certificate: %v", err)
			return subcommands.ExitFailure
		}
	}

	// Ensure that the CN is part of the subjectAltNames.
	c.sanList.Set(c.subject.Name.CommonName)

	switch {
	case aboutToExpire(cert, c.renewDays):
		fmt.Printf("certificate must be renewed (about to expire)\n")

	case !certificateMetadataEqual(cert, c.subject.Name, c.sanList, c.ipList, c.isClient, c.isServer):
		fmt.Printf("certificate must be renewed (metadata change)\n")

	case caCert != nil && !isSignedBy(cert, caCert):
		fmt.Printf("certificate must be renewed (signed by a different CA)\n")

	default:
		return subcommands.ExitSuccess
	}

	return subcommands.ExitFailure
}

func init() {
	subcommands.Register(&checkCmd{}, "")
}

var oneDay = 24 * time.Hour

func aboutToExpire(cert *x509.Certificate, renewDays int) bool {
	remaining := int(cert.NotAfter.Sub(time.Now().UTC()) / oneDay)
	return remaining < renewDays
}

func certificateMetadataEqual(cert *x509.Certificate, subject *pkix.Name, sanList []string, ipList []net.IP, isClient, isServer bool) bool {
	return (pkixEqual(cert.Subject, *subject) &&
		compareStringList(cert.DNSNames, sanList) &&
		compareIPList(cert.IPAddresses, ipList) &&
		(hasExtUsage(cert, x509.ExtKeyUsageClientAuth) == isClient) &&
		(hasExtUsage(cert, x509.ExtKeyUsageServerAuth) == isServer))
}

func isSignedBy(cert, ca *x509.Certificate) bool {
	return cert.CheckSignatureFrom(ca) == nil
}

func pkixEqual(a, b pkix.Name) bool {
	return (a.CommonName == b.CommonName &&
		compareStringList(a.Country, b.Country) &&
		compareStringList(a.Organization, b.Organization) &&
		compareStringList(a.OrganizationalUnit, b.OrganizationalUnit) &&
		compareStringList(a.Locality, b.Locality) &&
		compareStringList(a.Province, b.Province))
}

func hasExtUsage(cert *x509.Certificate, usage x509.ExtKeyUsage) bool {
	for _, u := range cert.ExtKeyUsage {
		if u == usage {
			return true
		}
	}
	return false
}

func compareStringList(a, b []string) bool {
	tmp := make(map[string]struct{})
	for _, elem := range a {
		tmp[elem] = struct{}{}
	}
	for _, elem := range b {
		if _, ok := tmp[elem]; !ok {
			return false
		}
		delete(tmp, elem)
	}
	return len(tmp) == 0
}

func compareIPList(a, b []net.IP) bool {
	tmp := make(map[string]struct{})
	for _, elem := range a {
		tmp[elem.String()] = struct{}{}
	}
	for _, elem := range b {
		s := elem.String()
		if _, ok := tmp[s]; !ok {
			return false
		}
		delete(tmp, s)
	}
	return len(tmp) == 0
}
