x509ca
======

A very simple command-line tool to generate arbitrary X509
certificates. Uses ECDSA and P256 curves.

## Usage

To initialize a Certification Authority:

    $ x509ca init --ca-cert ca.pem --ca-key ca.key --subject O=MyCA

To sign a new certificate:

    $ x509ca sign --ca-cert ca.pem --ca-key ca.key \
        --cert cert.pem --key key.pem --subject CN=myserver --server

Various options are available to customize the resulting certificate,
including setting subjectAltNames and IP addresses. See `x509ca sign
--help` for the full list.

If the certificate and private key referenced by the `--cert` and
`--key` options already exist, they will be regenerated only if they
are about to expire, or the certificate parameters have changed.

## Exit status

The exit status of *x509ca* is always 0 on success, 1 on error.

Both commands support the `--check` option, which will prevent the
program from writing anything to the filesystem, and will cause it to
exit with a status of 0 if the certificate already exists and would be
unchanged, 1 otherwise (certificate missing or expired).
