#!/bin/sh

die() {
    echo "ERROR: $*" >&2
    exit 1
}

x509ca() {
    echo "+ x509ca $*" >&2
    $tmpdir/x509ca "$@"
}

tmpdir=$(mktemp -d)
trap "trap - EXIT ; rm -fr $tmpdir ; exit" EXIT
go build -o $tmpdir/x509ca . || die "build failed"

cd $tmpdir
echo "using temporary directory $tmpdir"

x509ca init --subject=O=Test --ca-cert=ca_cert.pem --ca-key=ca_key.pem \
    || die "x509ca init failed"
x509ca gen-key --key=private_key.pem \
    || die "x509ca gen-key failed"

cert_subject="--subject=CN=test.service --alt=test2.service"
x509ca check --cert=cert.pem $cert_subject --server --renew-days=7 --ca-cert=ca_cert.pem \
    && die "x509ca check succeeded without a certificate file"
x509ca csr --key=private_key.pem $cert_subject \
    > csr.pem \
    || die "x509ca csr failed"
x509ca sign --ca-cert=ca_cert.pem --ca-key=ca_key.pem --server \
     < csr.pem > cert.pem \
    || die "x509ca sign failed"

x509ca check --cert=cert.pem $cert_subject --server --renew-days=7 --ca-cert=ca_cert.pem \
    || die "x509ca check failed with the newly generated certificate"

x509ca check --cert=cert.pem --subject=O=Different --renew-days=7 --ca-cert=ca_cert.pem \
    && die "x509ca check did not detect a subject change"

echo "OK" >&2
exit 0
