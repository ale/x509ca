package main

import (
	"crypto/x509/pkix"
	"fmt"
	"net"
	"strings"
)

type ipListFlag []net.IP

func (l ipListFlag) String() string {
	var out []string
	for _, ip := range l {
		out = append(out, ip.String())
	}
	return strings.Join(out, ",")
}

func (l ipListFlag) Contains(ip net.IP) bool {
	for _, elem := range l {
		if ip.Equal(elem) {
			return true
		}
	}
	return false
}

func (l *ipListFlag) Set(s string) error {
	ip := net.ParseIP(s)
	if ip == nil {
		return fmt.Errorf("could not parse IP: %s", s)
	}
	if !l.Contains(ip) {
		*l = append(*l, ip)
	}
	return nil
}

type stringListFlag []string

func (l *stringListFlag) String() string {
	return strings.Join(*l, ",")
}

func (l *stringListFlag) Contains(s string) bool {
	for _, elem := range *l {
		if elem == s {
			return true
		}
	}
	return false
}

func (l *stringListFlag) Set(s string) error {
	if !l.Contains(s) {
		*l = append(*l, s)
	}
	return nil
}

type pkixNameFlag struct {
	*pkix.Name
}

func (f *pkixNameFlag) PKIXName() pkix.Name {
	return *(f.Name)
}

func (f *pkixNameFlag) String() string {
	if f.Name == nil {
		return ""
	}
	return pkixNameToString(*f.Name)
}

func pkixNameToString(f pkix.Name) string {
	var out []string
	if f.CommonName != "" {
		out = append(out, "CN="+f.CommonName)
	}
	for _, s := range f.Country {
		out = append(out, "C="+s)
	}
	for _, s := range f.Organization {
		out = append(out, "O="+s)
	}
	for _, s := range f.OrganizationalUnit {
		out = append(out, "OU="+s)
	}
	for _, s := range f.Province {
		out = append(out, "ST="+s)
	}
	for _, s := range f.Locality {
		out = append(out, "L="+s)
	}
	return strings.Join(out, "/")
}

func (f *pkixNameFlag) Set(s string) error {
	var name pkix.Name
	for _, token := range strings.Split(s, "/") {
		if token == "" {
			continue
		}
		switch {
		case strings.HasPrefix(token, "CN="):
			name.CommonName = token[3:]
		case strings.HasPrefix(token, "C="):
			name.Country = append(name.Country, token[2:])
		case strings.HasPrefix(token, "O="):
			name.Organization = append(name.Organization, token[2:])
		case strings.HasPrefix(token, "OU="):
			name.OrganizationalUnit = append(name.OrganizationalUnit, token[3:])
		case strings.HasPrefix(token, "ST="):
			name.Province = append(name.Province, token[3:])
		case strings.HasPrefix(token, "L="):
			name.Locality = append(name.Locality, token[2:])
		default:
			return fmt.Errorf("unknown PKIX token '%s'", token)
		}
	}
	f.Name = &name
	return nil
}
